﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.IO.Streams;
using System.Reflection;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;

using TShockAPI;

using Terraria;
using TerrariaApi;
using TerrariaApi.Server;

using CustomNPCs.ConfigFiles;
using CustomNPCs.CustomisationFiles;
using CustomNPCs.CustomisationFiles.Events;

namespace CustomNPCs
{
    [ApiVersion(1, 15)]
    public class CustomNPC : TerrariaPlugin
    {
        public override string Author { get { return "White"; } }
        public override string Name { get { return "CustomNPCs"; } }
        public override string Description { get { return "Allows server owners to customise NPCs"; } }

        public static Tools Tools = Instancify<Tools>.Instance;
        public static SubCommandHandler Handler = Instancify<SubCommandHandler>.Instance;
        private EventManager manager;
        private MonsterManager monsters;

        [ThreadStatic]
        public static Random random = new Random();

        public override Version Version { get { return Assembly.GetExecutingAssembly().GetName().Version; } }

        public CustomNPC(Main game)
            : base(game)
        {
            manager = new EventManager();
            monsters = new MonsterManager(manager);
        }

        public override void Initialize()
        {
            ServerApi.Hooks.NpcLootDrop.Register(this, HandleLootDrop);
            ServerApi.Hooks.GameInitialize.Register(this, OnInitialize);
            ServerApi.Hooks.GamePostInitialize.Register(this, loadMonsters);
            ServerApi.Hooks.NetGetData.Register(this, OnGetData);
        }

        private void HandleLootDrop(NpcLootDropEventArgs args)
        {
            //Pish. Idk, my head hurts.
            foreach (CNPC c in Tools.cMonsters)
                if (c.npc.type == args.NpcId && !c.npc.active)
                    monsters.monsters.FirstOrDefault(m => m.Name == c.Name).LootDrop(args);
        }

        private void loadMonsters(EventArgs e)
        {
            monsters.LoadMonsters();

            ServerApi.Hooks.GamePostInitialize.Deregister(this, loadMonsters);
        }

        protected override void Dispose(bool disposing)
        {
            monsters.UnloadMonsters();
             
            if (disposing)
            {
                ServerApi.Hooks.NpcLootDrop.Deregister(this, HandleLootDrop);
                ServerApi.Hooks.GameInitialize.Deregister(this, OnInitialize);
                ServerApi.Hooks.NetGetData.Deregister(this, OnGetData);
            }
            base.Dispose(disposing);
        }

        #region OnInitialize
        void OnInitialize(EventArgs e)
        {
            //Tools.LoadConfig("CNPCs.json", "cnpc");
            //Tools.LoadConfig("Biomes.json", "biome");
            //Tools.LoadConfig("Transformers.json", "transform");

            /* Adds the base command "/cnpc" which is used to access the subcommands */
            Commands.ChatCommands.Add(new Command("cnpc.base", CNPC, "cnpc", "customnpc")
                {
                    HelpText = "Base command for the Custom NPC plugin."
                });

            /* Register a subcommand to the handler with permission "cnpc.base" and 4 command names, any of which can be used */
            Handler.RegisterSubcommand("cnpc.base", ListSubCommands, 
                "cmds", "commands", "subcommands", "subcmds");
            Handler.RegisterSubcommand("cnpc.base", SendList, "list");
            /* Register a subcommand to the handler with permission "cnpc.base.reload" and 1 command name */
            Handler.RegisterSubcommand("cnpc.base.reload", ReloadCNPCs, "reload");
            /* Register a subcommand to the handler with permission "cnpc.base.spawn" and 1 command name */
            Handler.RegisterSubcommand("cnpc.base.spawn", SpawnCommand, "spawn");
        }
        #endregion

        /// <summary>
        /// Sends a list of CNPCs that are spawnable
        /// </summary>
        /// <param name="args"></param>
        #region SendList
        private void SendList(CommandArgs args)
        {
            Tools.SendCNPCList(args.Parameters, args.Player);
        }
        #endregion

        /// <summary>
        /// Spawns CNPCs
        /// </summary>
        /// <param name="args">Arguments to be passed to the command</param>
        #region SpawnCNPCs
        private void SpawnCommand(CommandArgs args)
        {
            if (args.Parameters.Count > 0)
            {
                string cnpcID = args.Parameters[0];

                Log.Info("monster ID: " + args.Parameters[0]);

                List<string> Args = args.Parameters;
                args.Parameters.RemoveAt(0);

                Log.Info("Args removed");

                bool[] modparams = new bool[3] { false, false, false };
                for (int i = 0; i < args.Parameters.Count; i++)
                {
                    bool result;
                    if (bool.TryParse(args.Parameters[i], out result))
                        modparams[i] = result;
                }

                Log.Info("Modification parameters identified");

                var npcs = Tools.GetListOfNPCs(cnpcID);
                if (npcs.Count > 1)
                {
                    Log.Info("More than on custom monster found");
                    TShock.Utils.SendMultipleMatchError(args.Player, npcs.Select(n => n.Name));
                }
                else if (npcs.Count == 0)
                {
                    Log.Info("No custom monster found");
                    args.Player.SendErrorMessage("No custom npcs matched your query '{0}'", cnpcID);
                }
                else
                {
                    Log.Info("Attempting spawn");
                    Tools.SpawnNPC(npcs[0].Index, (int)args.Player.X + 48, (int)args.Player.Y, modparams[0],
                        modparams[1], modparams[2]);
                }
            }
            else
            {
                //TODO: If there are no args, then just send NPC list from here.
                //Tools.SendCNPCList(new List<string> { "1" }, args.Player)
            }
                
        }
        #endregion

        /// <summary>
        /// Reloads the base config file for CNPCS
        /// </summary>
        /// <param name="args">Arguments passed to the command</param>
        #region ReloadCNPCs
        private void ReloadCNPCs(CommandArgs args)
        {
            if (args.Parameters.Count > 0)
            {
                Tools.ReloadConfigs(args.Player, args.Parameters[0]);
            }
        }
        #endregion

        /// <summary>
        /// Sends the player a list of subcommands
        /// </summary>
        /// <param name="args">Arguments passed to the command</param>
        #region ListSubCommands
        private void ListSubCommands(CommandArgs args)
        {
            Tools.SendSubCommands(args.Parameters, args.Player);
        }
        #endregion

        /// <summary>
        /// Base command for the Custom NPC plugin. Will pass the arguments to the subcommand handler
        /// </summary>
        /// <param name="args">Arguments passed to the command</param>
        #region CNPC base command
        private void CNPC(CommandArgs args)
        {
            /* If the player submits more than one parameter (eg /cnpc stuff), it will attempt to handle the subcommand */
            if (args.Parameters.Count > 0)
                Handler.RunSubcommand(args);
            else
                Tools.SendSubCommands(args.Parameters, args.Player);
            /* Otherwise, it will send the player a list of the subcommands */
        }
        #endregion


        private void OnGetData(GetDataEventArgs e)
        {
            if (e.Handled)
                return;

            PacketTypes type = e.MsgID;
            var player = TShock.Players[e.Msg.whoAmI];
            if (player == null || !player.ConnectionAlive)
            {
                e.Handled = true;
                return;
            }

            using (var data = new MemoryStream(e.Msg.readBuffer, e.Index, e.Length))
            {
                if (type == PacketTypes.PlayerDamage)
                    OnPlayerDamage(new GetDataHandlerArgs(player, data));
                if (type == PacketTypes.NpcStrike)
                    OnNpcDamaged(new GetDataHandlerArgs(player, data));
            }
        }

        private void OnNpcDamaged(GetDataHandlerArgs args)
        {
            var npcid = args.Data.ReadInt16();
            var damage = args.Data.ReadInt16();
            args.Data.ReadSingle();
            args.Data.ReadInt8();
            var crit = args.Data.ReadBoolean();

            manager.InvokeHandler(new NPCDamageEvent() { Crit = crit, Damage = damage, PlayerIndex = args.Player.Index, NpcIndex = npcid }, EventType.NpcTakesDamage);

            int damageDone = damage - (int)Math.Ceiling(Main.npc[npcid].defense * .5f);
            damageDone = (crit ? damageDone * 2 : damageDone);
            if (damageDone >= Main.npc[npcid].life)
                manager.InvokeHandler(new NPCDeathEvent() { Crit = crit, Damage = damage, PlayerIndex = args.Player.Index, NpcIndex = npcid }, EventType.NpcIsKilled);
        }

        private void OnPlayerDamage(GetDataHandlerArgs args)
        {
            var id = args.Data.ReadInt8();
            args.Data.ReadInt8();
            var dmg = args.Data.ReadInt16();
            var pvp = args.Data.ReadBoolean();
            var crit = args.Data.ReadBoolean();

            manager.InvokeHandler(new PlayerDamageEvent() { Crit = crit, Damage = dmg, HurtPlayerIndex = args.Player.Index, DamagingEntityIndex = id, PVP = pvp }, EventType.PlayerTakesDamage);

            if (pvp)
                manager.InvokeHandler(new PlayerDamageEvent() { Crit = crit, Damage = dmg, HurtPlayerIndex = args.Player.Index, DamagingEntityIndex = id, PVP = pvp }, EventType.PlayerDoesDamage);
        }
    }
}

/*
 * Projectile proj = new Projectile();
 * proj.SetDefaults(int type);
 * proj.friendly = false;
 * proj.hostile = true;
 * NetMessage.SendData(27, -1, -1, "", proj.type, 0f, 0f, 0f, 0);
This should change the Projectile's status, then update it
 * This should allow setting minions to hostile, etc.
*/
