﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;

using TShockAPI;
using TShockAPI.DB;
using TShockAPI.Extensions;
using TShockAPI.Hooks;
using TShockAPI.Net;

namespace CustomNPCs
{
    /// <summary>
    /// Singleton class for creating single instances, rather than global variables
    /// </summary>
    /// <typeparam name="T">Class to use an instance of</typeparam>
    class Instancify<T> where T : class, new()
    {
        /// <summary>
        /// Generic class instance
        /// </summary>
        private static volatile T instance;

        private Instancify() { }

        /// <summary>
        /// Returns an instance of generic class T
        /// </summary>
        public static T Instance
        {
            get
            {
                if (instance == null)
                    instance = new T();

                return instance;
            }
        }
    }

    /// <summary>
    /// Handles subcommands
    /// </summary>
    public class SubCommandHandler
    {
        /// <summary>
        /// List of subcommands
        /// </summary>
        public List<SubCommand> subCommands = new List<SubCommand>();

        /// <summary>
        /// Registers the "help" subcommand
        /// </summary>
        public SubCommandHandler()
        {
            RegisterSubcommand("", DisplayHelpText, "help");
        }

        /// <summary>
        /// Sends the subcommand help text
        /// </summary>
        /// <param name="args"></param>
        private void DisplayHelpText(CommandArgs args)
        {
            CustomNPC.Tools.SendSubCommands(args.Parameters, args.Player);
        }

        /// <summary>
        /// Registers a subcommand that can be handled
        /// </summary>
        /// <param name="command">Name(s) of the command</param>
        /// <param name="func">Name of the void function which the command will invoke</param>
        /// <param name="permissions">What permissions the command requires to be run</param>
        public void RegisterSubcommand(List<string> permissions, Action<CommandArgs> func, params string[] commands)
        {
            subCommands.Add(new SubCommand(permissions, func, commands));
        }

        /// <summary>
        /// Registers a subcommand that can be handled
        /// </summary>
        /// <param name="permission">Permission the command requires to be run</param>
        /// <param name="func">Name of the void function which the command will invoke</param>
        /// <param name="commands">Name(s) of the command</param>
        public void RegisterSubcommand(string permission, Action<CommandArgs> func, params string[] commands)
        {
            subCommands.Add(new SubCommand(permission, func, commands));
        }

        /// <summary>
        /// Passes arguments to this handler and attempts to run the subcommand
        /// </summary>
        /// <param name="args">Original arguments from the command which includes the subcommand name</param>
        /// <variable name="newargs">New arguments taken from the original arguments- removes the first index position</variable>
        public void RunSubcommand(CommandArgs args)
        {
            CommandArgs newargs = new CommandArgs(args.Message, args.Player, args.Parameters.GetRange(1, args.Parameters.Count - 1));
            try
            {
                bool hasPermission = false;
                SubCommand subCommand = subCommands.Find(cmd => cmd.name.Contains(args.Parameters[0].ToLower()));
                
                if (subCommand == null)
                {
                    args.Player.SendErrorMessage("The subcommand entered is non existent.");
                    return;
                }

                foreach (string permission in subCommand.permissions)
                {
                    if (args.Player.Group.HasPermission(permission))
                        hasPermission = true;
                }

                if (hasPermission)
                {
                    subCommand.func.Invoke(newargs);
                }
            }
            catch (Exception e)
            {
                /* If executing the command fails, the user is notified. I then show them the list of subcommands */
                args.Player.SendErrorMessage("Command failed. Details can be found in the log");
                /* Push the error to the log */
                Log.Error(e.ToString());
                /* Invoke (execute) the help command */
                CustomNPC.Tools.SendSubCommands(new List<string> { "1" }, args.Player);
            }
        }
    }

    /// <summary>
    /// SubCommand class
    /// </summary>
    public class SubCommand
    {
        /// <summary>
        /// Permission(s) required to run the command
        /// </summary>
        public List<string> permissions;
        /// <summary>
        /// Name(s) that can be used to invoke the command
        /// </summary>
        public string[] name;
        /// <summary>
        /// CommandArgs void to be invoked
        /// </summary>
        public Action<CommandArgs> func;

        /// <summary>
        /// Class initializer with multiple permissions and commands
        /// </summary>
        /// <param name="permissions"></param>
        /// <param name="func"></param>
        /// <param name="commands"></param>
        public SubCommand(List<string> permissions, Action<CommandArgs> func, params string[] commands)
        {
            this.permissions = permissions;
            this.name = commands;
            this.func = func;
        }
        /// <summary>
        /// Class initializer with single permission and multiple commands
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="func"></param>
        /// <param name="command"></param>
        public SubCommand(string permission, Action<CommandArgs> func, params string[] command)
        {
            this.permissions = new List<string> { permission };
            this.name = command;
            this.func = func;
        }
        /// <summary>
        /// Class initializer with no permissions and one command
        /// </summary>
        /// <param name="func"></param>
        /// <param name="command"></param>
        public SubCommand(Action<CommandArgs> func, string command)
        {
            this.name[0] = command;
            this.func = func;
        }
    }
}