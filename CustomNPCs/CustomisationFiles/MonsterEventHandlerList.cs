﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    class MonsterEventHandlerList<T> : IEventHandlerList
    {
        readonly Dictionary<CustomMonster, Action<T>> handlers = new Dictionary<CustomMonster, Action<T>>();

        public void Register(CustomMonster c, object t)
        {
            lock (handlers)
            {
                if (handlers.ContainsKey(c))
                    handlers.Remove(c);

                handlers.Add(c, (Action<T>)t);
            }
        }

        public void Deregister(CustomMonster c)
        {
            lock (handlers)
            {
                if (handlers.ContainsKey(c))
                    handlers.Remove(c);
            }
        }

        public void Invoke(object args)
        {
            lock (handlers)
            {
                foreach (var handler in handlers.Values)
                    handler.Invoke((T)args);
            }
        }
    }
}
