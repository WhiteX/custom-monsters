﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles.Events
{
    public class PlayerDamageEvent
    {
        public int Damage { get; set; }
        public bool PVP { get; set; }
        public bool Crit { get; set; }
        public int HurtPlayerIndex { get; set; }
        public int DamagingEntityIndex { get; set; }
    }
}
