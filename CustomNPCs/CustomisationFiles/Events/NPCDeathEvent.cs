﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles.Events
{
    public class NPCDeathEvent
    {
        public int Damage { get; set; }
        public bool Crit { get; set; }
        public int NpcIndex { get; set; }
        public int PlayerIndex { get; set; }
    }
}
