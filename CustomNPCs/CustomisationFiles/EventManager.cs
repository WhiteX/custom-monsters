﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    class EventManager : IEventDispatcher, IEventRegister
	{
		public Dictionary<EventType, IEventHandlerList> handlerList = new Dictionary<EventType, IEventHandlerList>();

		public void InvokeHandler<T>(T args, EventType type)
		{
			if (!handlerList.ContainsKey(type))
				return;

			IEventHandlerList handlers = handlerList[type];
			handlers.Invoke(args);
		}

		public void DeregisterMonsterHandler(CustomMonster monster, EventType type)
		{
			if (!handlerList.ContainsKey(type))
				return;

			IEventHandlerList handlers = handlerList[type];

			handlers.Deregister(monster);
		}

		public void RegisterMonsterHandler<T>(CustomMonster monster, Action<T> handler, EventType type)
		{
			IEventHandlerList handlers;

			if (!handlerList.ContainsKey(type))
				handlerList.Add(type, new MonsterEventHandlerList<T>());

			handlers = handlerList[type];

			handlers.Register(monster, handler);
		}
	}
}
