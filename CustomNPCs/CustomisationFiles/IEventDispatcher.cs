﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    interface IEventDispatcher
    {
        void InvokeHandler<T>(T args, EventType type);
    }
}
