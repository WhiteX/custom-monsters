﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    interface IEventHandlerList
    {
        void Register(CustomMonster c, object t);
        void Deregister(CustomMonster c);
        void Invoke(object args);
    }
}
