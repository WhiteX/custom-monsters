﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Microsoft.CSharp;

using Terraria;
using TerrariaApi.Server;

using TShockAPI;

using CustomNPCs;
using CustomNPCs.ConfigFiles;

namespace CustomNPCs.CustomisationFiles
{
    public abstract class CustomMonster : IDisposable
    {
        public virtual string Name { get { return ""; } }

        abstract public void Initialize();


        public CustomMonster(IEventRegister register) { }

        ~CustomMonster()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) { }

        abstract public void LootDrop(NpcLootDropEventArgs args);

        public override bool Equals(object obj)
        {
            return GetType() == obj.GetType() && Equals((CustomMonster)obj);
        }

        public bool Equals(CustomMonster p)
        {
            return Name == p.Name;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}