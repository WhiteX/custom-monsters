﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    public enum EventType
    {
        PlayerTakesDamage,
        PlayerDoesDamage,
        NpcTakesDamage,
        NpcIsKilled
    }
}
