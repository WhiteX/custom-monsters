﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomNPCs.CustomisationFiles
{
    public interface IEventRegister
    {
        void DeregisterMonsterHandler(CustomMonster monster, EventType type);
        void RegisterMonsterHandler<T>(CustomMonster monster, Action<T> handler, EventType type);
    }
}
