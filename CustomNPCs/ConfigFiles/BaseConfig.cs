﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace CustomNPCs.ConfigFiles
{
    [Obsolete("Base config for code sample only")]
    public class BaseFile
    {
        public void Write(string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        public static BaseFile Read(string path)
        {
            if (!File.Exists(path))
                return new BaseFile();
            return JsonConvert.DeserializeObject<BaseFile>(File.ReadAllText(path));
        }
    }
}
