﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace CustomNPCs.ConfigFiles
{
    public class Event
    {
        public float TriggerRatio;
        public List<int[]> Transformations;
        //"hp" : 100
        public Dictionary<string, object> Flags = new Dictionary<string, object>();

        public Event(Dictionary<string, object> Flags, List<int[]> Transformations = null, float TriggerRatio = 0f)
        {
            this.Transformations = Transformations;
            this.TriggerRatio = TriggerRatio;
            this.Flags = Flags;
        }
    }
    public class TimeEvent
    {
        public float TriggerRatio;
        public List<int[]> Transformations;
        //"hp" : 100
        public Dictionary<string, object> Flags = new Dictionary<string, object>();

        public TimeEvent(Dictionary<string, object> Flags, List<int[]> Transformations = null, float TriggerRatio = 0f)
        {
            this.Transformations = Transformations;
            this.TriggerRatio = TriggerRatio;
            this.Flags = Flags;
        }
    }
    public class RetriggerEvent
    {
        public float TriggerRatio;
        public List<int[]> Transformations;
        //"hp" : 100
        public Dictionary<string, object> Flags = new Dictionary<string, object>();
        public RetriggerEvent(Dictionary<string, object> Flags, List<int[]> Transformations = null, float TriggerRatio = 0f)
        {
            this.Transformations = Transformations;
            this.TriggerRatio = TriggerRatio;
            this.Flags = Flags;
        }
    }
    public class NPCEvent
    {
        public List<Event> Events;
        public List<TimeEvent> TimeEvents;
        public List<RetriggerEvent> RetriggerEvents;
    }

    public class Customiser
    {
        public string CNPCNameOrID;
        public List<NPCEvent> NPCEvents;
		/* * = time event, ^ = re-trigger   [Should support both]
		* Syntax: "hp:(int)amt;eFunc,dam:(int)amt;eFunc,buffs:(int)type/(int)time;eFunc     "/" is an optional. Damage and hp should
		support ">" and "<" and basic maths symbols (eg "hp:>100" triggers when hp > 100, "hp:<100" triggers when hp < 100
		"hp:100" triggers when hp = 100, "hp:max - x" triggers when hp is maximum hp minus (int)x
		*/
		/* Time event syntax examples
		"hp:100;2s;eFunc" triggers when hp is at 100 for 2seconds, "dam:300;5s" triggers when 300 damage is taken over 2seconds
		"buff:type;time;eFunc" triggers when buff type is active for time seconds
		"time:seconds;eFunction" triggers a specific event function every seconds
		*/
		
		/*
		"type:(eFunc)heal/damage/buff/spawnMinions/speak/etc" 
		*/
		//For eFuncs: Add a handler, much like the subcommand handler. Load eFuncs for easier access.
			//void eFuncEvent(eFuncArgs args) { 
			//if (args.Parameters[0](lowered by default) == ("buff"/"hp"/"dam"/"time"/"etc")) { //Do func events }
			/* eFunc handler needs to have the name of the CNPCID it is registered too. */
		
        public Customiser(string CNPCNameOrID, List<NPCEvent> Events)
        {
            this.CNPCNameOrID = CNPCNameOrID;
            this.NPCEvents = Events;
        }
    }

    public class CustomisationConfig
    {
        public List<Customiser> Customisers = new List<Customiser>();

        public void Write(string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        public static CustomisationConfig Read(string path)
        {
            if (!File.Exists(path))
                return new CustomisationConfig();
            return JsonConvert.DeserializeObject<CustomisationConfig>(File.ReadAllText(path));
        } 
    }
}
