﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace CustomNPCs.ConfigFiles
{
    //Spawn timer to be able to use millisecond, second, minute, hour
    //Reset on leaving the area depending on config option
    //If false, pause the timer until someone enters it again


    //Split biomes with a comma for multiple: biome1,biome2,biome3

    public class RandomRatios
    {
        public float ratio;
        public string type;

        public RandomRatios(float ratio = 0.5f, string type = "spawnrate")
        {
            this.ratio = ratio;
            this.type = type;
        }
    }

    public class Biome
    {
        public bool ResetSpawnTimerOnAreaLeave;
        public string CNPCNameOrID;
        public string BiomeType;
        public float SpawnChance;
        public string SpawnAtTimeType;
        public string SpawnRate;
        public List<RandomRatios> randomRatios;

        public Biome(string CNPCNameOrID, string BiomeType, List<RandomRatios> randomRatios, float SpawnChance = 0.5f,
            string SpawnAtTimeType = "day", string SpawnRate = "1m20s", bool ResetSpawnTimerOnAreaLeave = false)
        {
            this.CNPCNameOrID = CNPCNameOrID;
            this.BiomeType = BiomeType;
            this.SpawnChance = SpawnChance;
            this.SpawnAtTimeType = SpawnAtTimeType;
            this.SpawnRate = SpawnRate;
            this.randomRatios = randomRatios;
            this.ResetSpawnTimerOnAreaLeave = ResetSpawnTimerOnAreaLeave;
        }
    }

    public class BiomeConfig
    {
        public List<Biome> BiomeCNPCs = new List<Biome>() 
        {
            new Biome("test1", "forest,sky", new List<RandomRatios>() { new RandomRatios() }, 0.6f, "all",
                "1m20s", true)
        };


        public void Write(string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        public static BiomeConfig Read(string path)
        {
            if (!File.Exists(path))
                return new BiomeConfig();
            return JsonConvert.DeserializeObject<BiomeConfig>(File.ReadAllText(path));
        }
    }
}
