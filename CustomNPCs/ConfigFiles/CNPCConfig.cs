﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace CustomNPCs.ConfigFiles
{
    public class CNPCConfig
    {
        public int MaxCNPCSpawns { get { return TShockAPI.TShock.Config.DefaultMaximumSpawns / 2; } }

        public List<CNPC> CustomNPCs = new List<CNPC>();

        public void Write(string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        public static CNPCConfig Read(string path)
        {
            if (!File.Exists(path))
                return new CNPCConfig();
            return JsonConvert.DeserializeObject<CNPCConfig>(File.ReadAllText(path));
        }
    }
}
