﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using TShockAPI;

using Terraria;
using TerrariaApi.Server;

using CustomNPCs.ConfigFiles;
using CustomNPCs.CustomisationFiles;

namespace CustomNPCs
{
    public class Tools
    {
        public List<CNPC> cMonsters = new List<CNPC>();
        public List<Biome> BiomeCNPCs = new List<Biome>();
        public List<Customiser> Customisations = new List<Customiser>();

        public CNPCConfig config { get; set; }
        public BiomeConfig bconfig { get; set; }
        public CustomisationConfig cconfig { get; set; }
        internal string DirectoryPath { get { return Path.Combine(TShock.SavePath, "CustomNPCs"); } }


        /* ~~~~~~ Lists ~~~~~~ */

        /// <summary>
        /// Sends a list of subcommands
        /// </summary>
        /// <param name="args">Parameters received from the command</param>
        /// <param name="player">The player to receive the subcommands</param>
        /// <param name="page">Which page of subcommands to view</param>
        #region SendSubCommands
        public void SendSubCommands(List<string> args, TSPlayer player)
        {
            int Page = 1;
            if (!PaginationTools.TryParsePageNumber(args, 0, player, out Page))
                return;
            else
            {
                /* Get the list of subcommands */
                var subCommands = CustomNPC.Handler.subCommands;

                /* Create a List<String> of the subcommand names */
                var commands = subCommands.Select(s => String.Join(", ", s.name)).ToList();

                /* Send the player a page of subcommands, relative to the page from the method */
                PaginationTools.SendPage(player, Page, commands, new PaginationTools.Settings
                    {
                        HeaderFormat = "Subcommands of /cnpc   ~Page {0}/{1}~",
                        HeaderTextColor = Color.Lime,
                        FooterFormat = "Use /cnpc cmds {0} for more",
                        FooterTextColor = Color.Lime,
                        LineTextColor = Color.White,
                        NothingToDisplayString = "No subcommands have been added yet.",
                        MaxLinesPerPage = 5
                    });
            }
        }
        #endregion

        /// <summary>
        /// Sends a list of alive and available CNPCs
        /// </summary>
        /// <param name="receiver">Player to receive the list</param>
        #region SendList
        [Obsolete("This method is broken at the moment")]
        public void SendCNPCList(List<String> args, TSPlayer receiver)
        {
            PaginationTools.Settings settings = new PaginationTools.Settings
            {
                HeaderFormat = "Spawnable CNPCs ({0}/{1})",
                HeaderTextColor = Color.Lime,
                FooterFormat = "Use /cnpc list {0} for more",
                FooterTextColor = Color.Lime,
                LineTextColor = Color.White,
                NothingToDisplayString = "No CNPCs have been defined"
            };

            List<CNPC> monsterList = new List<CNPC>();
            //This needs fixing.
            int Page;
            if (PaginationTools.TryParsePageNumber(args, 0, receiver, out Page))
            {
                foreach (CNPC m in cMonsters)
                {
                    monsterList.Add(m);
                }
                //foreach (CNPC cnpc in cMonsters)
                //{
                //    for (int i = 0; i < Main.npc.Length; i++)
                //    {
                //        if (Main.npc[i].type == cnpc.baseNPC)
                //            if (Main.npc[i].active)
                //            {
                //                if (!CNPCList.ContainsKey(cnpc.Uname))
                //                    CNPCList.Add(cnpc.Uname, " ID: " + " (alive)");
                //            } //Add ID back in sometime
                //            else
                //                if (!CNPCList.ContainsKey(cnpc.Uname))
                //                    CNPCList.Add(cnpc.Uname, " ID: ");
                //    }
                //}
                //PaginationTools.SendPage(receiver, Page, CNPCDictToList(CNPCList), settings);
            }
            else
                PaginationTools.SendPage(receiver, Page, monsterList, settings);
        }
        #endregion

        /* ~~~~~~ /Lists\ ~~~~~~ */


        /* ~~~~~~ CNPC methods ~~~~~~ */

        public CNPC GetNPC(int index)
        {
            lock (cMonsters)
            {
                if (cMonsters[index] == null)
                    throw new InvalidOperationException(String.Format("Monster at index {0} is null.", index));

                return cMonsters[index];
            }
        }

        public List<CNPC> GetListOfNPCs(string name)
        {
            var npcs = new List<CNPC>();
            foreach (CNPC npc in cMonsters)
            {
                if (npc.Name.ToLower() == name.ToLower())
                    return new List<CNPC>() { npc };

                if (npc.Name.ToLower().Contains(name.ToLower()))
                    npcs.Add(npc);
            }

            return npcs;
        }

        /// <summary>
        /// Spawns a Custom Monster
        /// </summary>
        /// <param name="cnpc">The Custom Monster to spawn</param>
        /// <param name="X">Where to spawn it (x, ?)</param>
        /// <param name="Y">Where to spawn it (?, y)</param>
        #region SpawnNPC
        public void SpawnNPC(int index, int X, int Y, bool immuneToDamage = false, bool immuneToLava = false,
            bool noGravity = false, bool noTileCollide = false)
        {
            /* Create a new NPC at position (x, y) with baseType relative to that of the Custom NPC */
            CNPC custom = cMonsters[index];
            int npcID = NPC.NewNPC(X, Y, custom.baseNPC);
            /* Set its default values */
            Main.npc[npcID].SetDefaults(custom.baseNPC);
            /* Customise it */
            CreateCNPC(npcID, custom);
        }
        #endregion

        /// <summary>
        /// Customizes an NPC into a Custom Monster
        /// </summary>
        /// <param name="npcID">ID of the NPC to customize</param>
        /// <param name="cnpc">Custom Monster to customize the NPC with</param>
        #region CreateCNPC
        public void CreateCNPC(int npcID, CNPC custom, bool immuneToDamage = false, bool immuneToLava = false,
            bool noGravity = false, bool noTileCollide = false)
        {
            /* Find the NPC */
            NPC npc = Main.npc[npcID];
            /* Set its default values */
            npc.netDefaults(custom.baseNPC);

            custom.npc = npc;

            /* Update all its values to reflect the ones in the custom NPC */
            npc.name = (custom.Name != string.Empty ? custom.Name : npc.name);
            npc.displayName = (custom.Name != string.Empty ? custom.Name : npc.name);
            npc.damage = (custom.Damage != -1 ? custom.Damage : npc.damage);
            npc.defense = (custom.Defense != -1 ? custom.Defense : npc.defense);
            npc.lifeMax = (custom.Health != -1 ? custom.Health : npc.lifeMax);
            npc.life = npc.lifeMax;
            npc.aiStyle = (custom.AIStyle != -1 ? custom.AIStyle : npc.aiStyle);
            npc.aiStyle = (custom.AIStyle != -1 ? custom.AIStyle : npc.aiStyle);
            npc.boss = custom.Boss;

            if (immuneToLava)
                npc.lavaImmune = true;
            if (immuneToDamage)
                npc.dontTakeDamage = true;
            if (noTileCollide)
                npc.noTileCollide = true;
            if (noGravity)
                npc.noGravity = true;

            /* Send an NPC update packet to update the NPC */
            NetMessage.SendData(23, -1, -1, "", npcID, 0f, 0f, 0f, 0);

            /* If its been tagged as a boss, send the spawn message if there is one, or {name} has awoken. if there isn't */
            if (custom.Boss)
                foreach (string message in custom.SpawnMessage.Split('|'))
                {
                    TSPlayer.All.SendMessage(message != string.Empty ? message :
                        npc.name + " (" + Main.npc[npcID].name + ") " + " has awoken.", Color.MediumPurple);
                }
        }
        #endregion

        public void TransformCustomNoReSpawn(CNPC custom, int newNPC, int lifeAddition = 0)
        {

        }

        /// <summary>
        /// Transform the NPC. Can add life
        /// </summary>
        /// <param name="custom"></param>
        /// <param name="newNPC"></param>
        /// <param name="lifeAddition"></param>
        public void TransformCustom(CNPC custom, int newNPC, int lifeAddition = 0)
        {
            int npcID = NPC.NewNPC((int)custom.npc.position.X, (int)custom.npc.position.Y, newNPC);
            if (npcID > 200)
                return;
            NPC npc = Main.npc[npcID];
            /* Set its default values */
            npc.netDefaults(custom.baseNPC);


            /* Update all its values to reflect the ones in the custom NPC */
            npc.name = (custom.Name != string.Empty ? custom.Name : npc.name);
            npc.damage = (custom.Damage != -1 ? custom.Damage : npc.damage);
            npc.defense = (custom.Defense != -1 ? custom.Defense : npc.defense);
            npc.lifeMax = (custom.Health != -1 ? custom.Health : npc.lifeMax);
            npc.life = (lifeAddition != 0 ? custom.npc.life + lifeAddition : npc.lifeMax);
            npc.aiStyle = (custom.AIStyle != -1 ? custom.AIStyle : npc.aiStyle);
            npc.aiStyle = (custom.AIStyle != -1 ? custom.AIStyle : npc.aiStyle);
            npc.boss = custom.Boss;

            custom.npc.active = false;

            custom.npc = npc;


            /* Send an NPC update packet to update the NPC */
            NetMessage.SendData(23, -1, -1, "", newNPC, 0f, 0f, 0f, 0);

            //TODO: Add a customisable transformation message.
        }

        /// <summary>
        /// Update a custom monster. Optional life addition
        /// </summary>
        /// <param name="index">Index of the custom monster to use</param>
        /// <param name="lifeAddition"></param>
        public void UpdateCustom(int index, int? lifeAddition = null)
        {
            CNPC custom = cMonsters[index];
            if (lifeAddition != null)
                custom.npc.life += (int)lifeAddition;

            NetMessage.SendData(23, -1, -1, "", custom.npc.type, 0f, 0f, 0f, 0);
        }

        /* ~~~~~~ /CNPC methods\ ~~~~~~ */


        public CNPC NewCNPC(int baseNPC, int damage = 100, int health = 10000, int defense = 100, float MoveSpeed = 1f,
            bool boss = false, int AI = -1, string name = null, string Uname = null, string SpawnMessage = null)
        {
            CNPC custom = new CNPC(1, baseNPC, damage, health, defense, MoveSpeed, boss,
                AI, name, Uname, SpawnMessage);
            cMonsters.Add(custom);
            return custom;
        }


        /* ~~~~~~ Load methods ~~~~~~ */

        #region ReloadConfigs
        public void ReloadConfigs(TSPlayer receiver, string type)
        {
            List<string> Errors = new List<string>();
            int count = 0;

            switch (type.ToLower())
            {
                case "cnpc":
                case "cnpcs":
                    {
                        string configPath = Path.Combine(DirectoryPath, "CNPCs.json");
                        (config = CNPCConfig.Read(configPath)).Write(configPath);
                        //CNPCs.Clear();

                        if (config.CustomNPCs.Count > 0)
                        {
                            foreach (CNPC cnpc in config.CustomNPCs)
                            {
                                try
                                {
                                    //CNPCs.Add(new CNPC(cnpc.IDnumber, (int)cnpc.baseNPC, cnpc.Damage, cnpc.Health, cnpc.Defense, cnpc.MoveSpeed,
                                    //    cnpc.Boss, cnpc.AIStyle, cnpc.Name, cnpc.Uname, cnpc.SpawnMessage));
                                    count++;
                                }
                                catch (Exception ex)
                                {
                                    Errors.Add(cnpc.Name + " was not initialized due to errors: " + ex.Message);
                                }
                            }
                        }
                        if (count > 0)
                            Log.ConsoleInfo("Successfully loaded {0} Custom NPC{1}", count,
                                count > 1 ? "s" : "");
                        else
                            Log.ConsoleError("No Custom NPCs loaded as none were defined");
                        if (Errors.Count == 1)
                        {
                            Log.ConsoleError("1 error encountered while loading Custom NPCs:");
                            Log.ConsoleError(Errors[0]);
                        }
                        else if (Errors.Count > 1)
                        {
                            Log.ConsoleError("{0} errors encountered while loading Custom NPCs:", Errors.Count);
                            Log.ConsoleError("'" + string.Join("', '", Errors));
                        }

                        receiver.SendSuccessMessage("Reloaded Custom NPCs.{0}",
                            Errors.Count > 0 ? " Errors occured: " + Errors.Count : "");

                        break;
                    }
                case "biome":
                case "biomes":
                    {
                        string configPath = Path.Combine(DirectoryPath, "Biomes.json");
                        (bconfig = BiomeConfig.Read(configPath)).Write(configPath);
                        BiomeCNPCs.Clear();

                        if (bconfig.BiomeCNPCs.Count > 0)
                        {
                            foreach (Biome b in bconfig.BiomeCNPCs)
                            {
                                try
                                {
                                    BiomeCNPCs.Add(new Biome(b.CNPCNameOrID, b.BiomeType, b.randomRatios, b.SpawnChance,
                                        b.SpawnAtTimeType, b.SpawnRate, b.ResetSpawnTimerOnAreaLeave));
                                    count++;
                                }
                                catch (Exception ex)
                                {
                                    Errors.Add(b.CNPCNameOrID + " was not initialized due to errors: " + ex.Message);
                                }
                            }

                            if (count > 0)
                                Log.ConsoleInfo("Successfully loaded {0} Biome definition{1}", count,
                                    count > 1 ? "s" : "");
                            else
                                Log.ConsoleError("No Biome definitions loaded as none were defined");
                            if (Errors.Count == 1)
                            {
                                Log.ConsoleError("1 error encountered while loading Biome definitions:");
                                Log.ConsoleError(Errors[0]);
                            }
                            else if (Errors.Count > 1)
                            {
                                Log.ConsoleError("{0} errors encountered while loading Biome definitions:", Errors.Count);
                                Log.ConsoleError("'" + string.Join("', '", Errors));
                            }

                            receiver.SendSuccessMessage("Reloaded Biome definitions.{0}",
                                Errors.Count > 0 ? " Errors occured: " + Errors.Count : "");
                        }
                        break;
                    }
                case "transform":
                case "transformers":
                    {
                        string configPath = Path.Combine(DirectoryPath, "Transformers.json");
                        (cconfig = CustomisationConfig.Read(configPath)).Write(configPath);
                        Customisations.Clear();

                        if (cconfig.Customisers.Count > 0)
                        {
                            foreach (Customiser t in cconfig.Customisers)
                            {
                                try
                                {
                                    Customisations.Add(new Customiser(t.CNPCNameOrID, t.NPCEvents));
                                }
                                catch (Exception ex)
                                {
                                    Errors.Add(t.CNPCNameOrID + " was not initialized due to errors: " + ex.Message);
                                }
                            }
                            if (count > 0)
                                Log.ConsoleInfo("Successfully loaded {0} Transformer definition{1}", count,
                                    count > 1 ? "s" : "");
                            else
                                Log.ConsoleError("No Transformer definitions loaded as none were defined");
                            if (Errors.Count == 1)
                            {
                                Log.ConsoleError("1 error encountered while loading Transformer definitions:");
                                Log.ConsoleError(Errors[0]);
                            }
                            else if (Errors.Count > 1)
                            {
                                Log.ConsoleError("{0} errors encountered while loading Transformer definitions:",
                                    Errors.Count);
                                Log.ConsoleError("'" + string.Join("', '", Errors));
                            }

                            receiver.SendSuccessMessage("Reloaded Transformer definitions.{0}",
                                Errors.Count > 0 ? " Errors occured: " + Errors.Count : "");
                        }
                        break;
                    }
                default:
                    {
                        receiver.SendErrorMessage("Invalid reload option. Valid options: 'cnpc', 'biome', 'transform'");
                        break;
                    }
            }






        }
        #endregion

        /// <summary>
        /// Loads config files
        /// </summary>
        /// <param name="filePath">Name of the file (CNPCs.json, etc)</param>
        /// <param name="type">Type of file ("cnpc", "biome", "transform")</param>
        #region LoadConfig
        public void LoadConfig(string filePath, string type)
        {
            bool ConfigInitialized = false;

            string DirectoryPath = this.DirectoryPath;
            CNPCConfig config = this.config;
            BiomeConfig bconfig = this.bconfig;
            CustomisationConfig tconfig = this.cconfig;

            /* Checks if the base directory exists */
            if (Directory.Exists(DirectoryPath))
            {
                /* Initializes the config file */
                string configPath = Path.Combine(DirectoryPath, filePath);
                if (type == "cnpc")
                    (config = CNPCConfig.Read(configPath)).Write(configPath);
                if (type == "biome")
                    (bconfig = BiomeConfig.Read(configPath)).Write(configPath);
                if (type == "transform")
                    (tconfig = CustomisationConfig.Read(configPath)).Write(configPath);
                ConfigInitialized = true;
            }
            else
            {
                /* Attempts to create the base directory because it doesn't exist. */
                try
                {
                    Directory.CreateDirectory(DirectoryPath);
                    string configPath = Path.Combine(DirectoryPath, filePath);
                    if (type == "cnpc")
                        (config = CNPCConfig.Read(configPath)).Write(configPath);
                    if (type == "biome")
                        (bconfig = BiomeConfig.Read(configPath)).Write(configPath);
                    if (type == "transform")
                        (tconfig = CustomisationConfig.Read(configPath)).Write(configPath);
                    ConfigInitialized = true;
                }
                /* If the above logic fails, outputs to log and refuses to populate the CNPCs */
                catch (Exception ex)
                {
                    ConfigInitialized = false;
                    /* Push the error to the log and console */
                    Log.ConsoleError(ex.ToString());
                }
            }

            if (ConfigInitialized)
                return;
            else
                Errors(ConfigInitialized);
        }
        #endregion


        /// <summary>
        /// Sends errors if configs are initialized unsuccessfully
        /// </summary>
        /// <param name="ConfigInitialized">True or false</param>
        #region Errors
        void Errors(bool ConfigInitialized)
        {
            /* Config initialized successfully, so populate the CNPC list */
            if (ConfigInitialized)
            {
                /* Number of CNPCs found */
                int count = 0;
                /* List of errors encountered */
                List<string> Errors = new List<string>();

                /* Checks if the list of CNPCs in the config has 1 or more CNPCs in it. No point doing things otherwise */
                if (config.CustomNPCs.Count > 0)
                {
                    /* Iterate through the list */
                    foreach (CNPC cnpc in config.CustomNPCs)
                    {
                        try
                        {
                            /* Attempt to add the config CNPC to the code CNPC list, where it can be used */
                            //CNPCs.Add(new CNPC(cnpc.IDnumber, (int)cnpc.baseNPC, cnpc.Damage, cnpc.Health, cnpc.Defense, cnpc.MoveSpeed,
                            //    cnpc.Boss, cnpc.AIStyle, cnpc.Name, cnpc.Uname, cnpc.SpawnMessage));
                            count++;
                        }
                        catch (Exception ex)
                        {
                            /* Adds an error if adding the CNPC failed */
                            Errors.Add(cnpc.Name + " was not initialized due to errors: " + ex.Message);
                        }
                    }
                }
                /* Output how many CNPCs were made  [SHOULD BE IN POSTINITIALIZE OR SOMESUCH] */
                if (count > 0)
                    Console.WriteLine("Successfully loaded {0} Custom NPC{1}", count,
                        count > 1 || count == 0 ? "s" : "");
                /* Error message to let user know that CNPCs plugin has been loaded, but no CNPCs were defined */
                else
                    Log.ConsoleError("No Custom NPCs loaded as none were defined");

                /* If 1 error was recorded: */
                if (Errors.Count == 1)
                {
                    /* Write it to the console */
                    Console.WriteLine("1 error encountered while loading Custom NPCs:");
                    Console.WriteLine(Errors[0]);
                }
                /* If multiple errors were recorded */
                else if (Errors.Count > 1)
                {
                    /* Write them all to the console */
                    Console.WriteLine("{0} errors encountered while loading Custom NPCs:", Errors.Count);
                    Console.WriteLine("'" + string.Join("', '", Errors));
                }
            }
        }
        #endregion

        /* ~~~~~~ /Load methods\ ~~~~~~ */



        /* ~~~~~~ Tools ~~~~~~ */


        public void HandleLootDrop(DropEditor dropEditor, NpcLootDropEventArgs args, int NpcID)
        {
            if (args.NpcId == NpcID)
            {
                if (Main.bloodMoon && dropEditor.drops.ContainsKey(State.Bloodmoon))
                {
                    foreach (Drop d in dropEditor.drops[State.Bloodmoon])
                    {
                        double rng = CustomNPC.random.NextDouble();
                        if (d.chance >= rng)
                        {
                            var item = TShock.Utils.GetItemById(d.itemID);
                            int stack = CustomNPC.random.Next(d.low_stack, d.high_stack + 1);
                            Item.NewItem(args.X, args.Y, item.width, item.height, d.itemID, stack, args.Broadcast, d.prefix);

                            args.Handled = true;

                            if (!dropEditor.tryEachItem)
                                break;
                        }
                    }
                }

                if (Main.eclipse && dropEditor.drops.ContainsKey(State.Eclipse))
                {
                    foreach (Drop d in dropEditor.drops[State.Eclipse])
                    {
                        double rng = CustomNPC.random.NextDouble();
                        if (d.chance >= rng)
                        {
                            var item = TShock.Utils.GetItemById(d.itemID);
                            int stack = CustomNPC.random.Next(d.low_stack, d.high_stack + 1);
                            Item.NewItem(args.X, args.Y, item.width, item.height, d.itemID, stack, args.Broadcast, d.prefix);

                            args.Handled = true;

                            if (!dropEditor.tryEachItem)
                                break;
                        }
                    }
                }

                if (Main.moonPhase == 0 && !Main.dayTime && dropEditor.drops.ContainsKey(State.Fullmoon))
                {
                    foreach (Drop d in dropEditor.drops[State.Fullmoon])
                    {
                        double rng = CustomNPC.random.NextDouble();
                        if (d.chance >= rng)
                        {
                            var item = TShock.Utils.GetItemById(d.itemID);
                            int stack = CustomNPC.random.Next(d.low_stack, d.high_stack + 1);
                            Item.NewItem(args.X, args.Y, item.width, item.height, d.itemID, stack, args.Broadcast, d.prefix);

                            args.Handled = true;

                            if (!dropEditor.tryEachItem)
                                break;
                        }
                    }
                }

                foreach (Drop d in dropEditor.drops[State.Normal])
                {
                    double rng = CustomNPC.random.NextDouble();
                    if (d.chance >= rng)
                    {
                        var item = TShock.Utils.GetItemById(d.itemID);
                        int stack = CustomNPC.random.Next(d.low_stack, d.high_stack + 1);
                        Item.NewItem(args.X, args.Y, item.width, item.height, d.itemID, stack, args.Broadcast, d.prefix);
                        args.Handled = true;

                        if (!dropEditor.tryEachItem)
                            break;
                    }
                }
                if (Main.dayTime && dropEditor.drops.ContainsKey(State.Day))
                {
                    foreach (Drop d in dropEditor.drops[State.Day])
                    {
                        double rng = CustomNPC.random.NextDouble();
                        if (d.chance >= rng)
                        {
                            var item = TShock.Utils.GetItemById(d.itemID);
                            int stack = CustomNPC.random.Next(d.low_stack, d.high_stack + 1);
                            Item.NewItem(args.X, args.Y, item.width, item.height, d.itemID, stack, args.Broadcast, d.prefix);

                            args.Handled = true;

                            if (!dropEditor.tryEachItem)
                                break;
                        }
                    }
                }
            }
        }

        /* ~~~~~~ /Tools\ ~~~~~~ */
    }

    /* Thanks Zack */
    public class DropEditor
    {
        public Dictionary<State, List<Drop>> drops;
        public bool tryEachItem = true;
        public bool alsoDropDefaultLoot = false;

        public DropEditor()
        {
            drops = new Dictionary<State, List<Drop>>();
        }
    }

    public enum State
    {
        Normal,
        Bloodmoon,
        Eclipse,
        Night,
        Day,
        Fullmoon
    }

    public class Drop
    {
        public float chance = 1.0f;

        public int low_stack = 0;
        public int high_stack = 1;

        public int itemID;

        public int prefix;

        public Drop() { }
    }
}
