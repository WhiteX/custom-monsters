﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Terraria;
using TShockAPI;

namespace CustomNPCs
{
    /* The actual custom NPC class */
    public class CNPC
    {
        public int[] Transformations;
        public int TimesToTransform = 1;
        /// <summary>
        /// Index of the monster inside the monster array
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Whether or not the NPC can be spawned
        /// </summary>
        public bool AllowSpawns;
        /// <summary>
        /// How fast the CNPC should move
        /// </summary>
        public float MoveSpeed = 1f;
        /// <summary>
        /// Custom AI style for the CNPC
        /// </summary>
        public int AIStyle = -1;
        /// <summary>
        /// Custom name of the NPC
        /// </summary>
        public string Name = string.Empty;
        /// <summary>
        /// The NPC's ID that the CNPC is based on
        /// </summary>
        public int baseNPC = 0;

        public NPC npc { get; set; }
        /// <summary>
        /// Unique spawning name of the NPC
        /// </summary>
        public string Uname;
        /// <summary>
        /// Whether or not the NPC is a boss
        /// </summary>
        public bool Boss = false;
        /// <summary>
        /// Message to be sent when the npc spawns, if any
        /// </summary>
        public string SpawnMessage = string.Empty;
        /// <summary>
        /// Percentage chance of the CNPC replacing a normal NPC (1f = 100%)
        /// </summary>
        public float ReplaceChance = 0.5f;
        /// <summary>
        /// Maximum number of CNPC that can be spawned before AllowSpawns becomes false
        /// </summary>
        public int MaxSpawns = -1;

        /// <summary>
        /// What projectile the CNPC shoots
        /// </summary>
        public int Projectile = -1;
        /// <summary>
        /// The amount of health the CNPC has
        /// </summary>
        public int Health = -1;
        /// <summary>
        /// The amount of defense the CNPC has
        /// </summary>
        public int Defense = -1;
        /// <summary>
        /// The amount of damage the CNPC deals
        /// </summary>
        public int Damage = -1;

        public Vector2 Position { get; set; }

        [Obsolete("This method is currently not working")]
        public InflictBuffs Buffs;

        /// <summary>
        /// Class initializer for CNPC
        /// </summary>
        /// <param name="IDnumber">Index position of the CNPC in the CNPC list</param>
        /// <param name="baseNPC">NPC type to base the CNPC on</param>
        /// <param name="damage">Damage the CNPC should inflict</param>
        /// <param name="health">Health the CNPC should have</param>
        /// <param name="defense">Defense the CNPC should have</param>
        /// <param name="boss">Boolean value that sets whether the CNPC is a boss or not</param>
        /// <param name="name">Override for the CNPC's name</param>
        /// <param name="Uname">Unique name to spawn the CNPC with (supports int values)</param>
        /// <param name="SpawnMessage">Message to be sent on spawning if CNPC is a boss</param>
        /// <param name="buffs">Buffs the CNPC can inflict [Not implemented]</param>
        /// <param name="buffTimes">How long the buffs shall be inflicted for [Not implemented]</param>
        public CNPC(int Index, int baseNPC, int damage = 100, int health = 10000, int defense = 100, float MoveSpeed = 1f,
            bool boss = false, int AI = -1, string name = null, string Uname = null, string SpawnMessage = null,
            int[] buffs = null, int[] buffTimes = null)
        {
            this.baseNPC = baseNPC;
            this.npc = Main.npc[NPC.NewNPC(0, 0, baseNPC)];
            this.Index = Index;

            Damage = damage;

            this.AIStyle = AI;

            if (name != null)
                Name = name;

            Health = health;

            Defense = defense;

            Boss = boss;

            if (SpawnMessage != null)
                this.SpawnMessage = SpawnMessage;

            if (Uname != null)
                this.Uname = Uname;

            this.MoveSpeed = MoveSpeed;

            this.Position = Vector2.Zero;
        }
    }

    /// <summary>
    /// Class of buffs and buff times that the mob will inflict
    /// </summary>
    [Obsolete("This method is currently not working")]
    public class InflictBuffs
    {
        /// <summary>
        /// Buffs[0] has Time[0]
        /// </summary>
        public string Buffs = string.Empty;
        /// <summary>
        /// Time[0] has Buffs[0]
        /// </summary>
        public string Times = string.Empty;

        public InflictBuffs(string Buffs, string Times)
        {
            this.Buffs = Buffs;
            this.Times = Times;
        }
    }
}
