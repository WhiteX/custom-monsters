﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using CustomNPCs;
using CustomNPCs.ConfigFiles;
using CustomNPCs.CustomisationFiles;
using CustomNPCs.CustomisationFiles.Events;

using Terraria;
using TerrariaApi.Server;

using TShockAPI;

/* [IMPROVEMENT] Instancify this class, meaning that every time one spawns, it's a new instance. This allows for better
 * management of each NPC */

/* [BUG] Spawning a normal NPC turns that NPC into a custom npc if it has the same type */

namespace TestNPC
{
    //TNPC (Test NPC) is a custom monster extension
    public class TNPC : CustomMonster
    {
        public static int transformations = 0;
        //EventRegister interface instance
        public IEventRegister register;
        public override string Name { get { return "Test"; } }

        //The Custom monster this extension represents
        public CNPC npc { get; set; }

        public TNPC instance;

        public DropEditor dropEditor;

        //Constructor
        public TNPC(IEventRegister register)
            : base(register)
        {
            this.register = register;
            dropEditor = new DropEditor()
            {
                alsoDropDefaultLoot = false,
                tryEachItem = true,
                drops = new Dictionary<State, List<Drop>>()
            };
        }

        public override void Initialize()
        {
            //Register a handler for when the Monster takes damage
            register.RegisterMonsterHandler<NPCDamageEvent>(this, CNPCReceiveDamage, EventType.NpcTakesDamage);
            register.RegisterMonsterHandler<NPCDeathEvent>(this, CNPCDies, EventType.NpcIsKilled);

            //Set the value of this.npc
            npc = CustomNPC.Tools.NewCNPC(32, 100, 100, 10, 1f, true, -1, "Test", this.Name, "u wot m8");
            npc.Transformations = new int[5] { 46, 45, 44, 47, 49};

            Drop d = new Drop();
            d.itemID = 1;
            d.chance = 1.0f;
            d.high_stack = 999;
            d.low_stack = 0;
            dropEditor.drops.Add(State.Normal, new List<Drop>() { d });
        }

        public override void LootDrop(NpcLootDropEventArgs args)
        {
            CustomNPC.Tools.HandleLootDrop(dropEditor, args, npc.npc.type);
        }

        private void CNPCDies(NPCDeathEvent args)
        {
            Console.WriteLine("Transformations reset");
            transformations = 0;
        }



        /// <summary>
        /// Method that is fired when this monster receives damage
        /// </summary>
        /// <param name="args">Damage event args passed to the command</param>
        public void CNPCReceiveDamage(NPCDamageEvent args)
        {
            //Checks if the npc's current life is less than half its life
            if (npc.npc.life < npc.npc.lifeMax / 2)
            {
                //Check if the npc can actually transform
                if (npc.Transformations.Length > 0 && transformations < npc.TimesToTransform)
                {
                    //if it is, get a random transformation from the list of transformations that the custom monster has

                    int i = CustomNPC.random.Next(0, npc.Transformations.Length);
                    //Then transform the custom monsters
                    npc.Transform(npc.Transformations[i]);
                    transformations++;
                    Console.WriteLine(transformations);
                }
            }

            //Updates the custom NPC, restoring 1/4 of its life
            CustomNPC.Tools.UpdateCustom(npc.Index);
            Console.WriteLine(npc.Heal(npc.npc.life / 4));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //Dispose the handler
                register.DeregisterMonsterHandler(this, EventType.NpcTakesDamage);
                register.DeregisterMonsterHandler(this, EventType.NpcIsKilled);
            }
        }
    }

    //Extension class
    public static class CNPCex
    {
        //Extend CNPC, adding a Transform void
        public static void Transform(this CNPC monster, int transformation)
        {
                Console.WriteLine("Transform event triggered");
                CustomNPC.Tools.TransformCustom(monster, transformation, monster.npc.lifeMax / 4);
        }
        //Works
        /// <summary>
        /// Heals monster
        /// </summary>
        /// <param name="monster"></param>
        /// <param name="life">Amount to heal</param>
        /// <returns>Amount healed</returns>
        public static int Heal(this CNPC monster, int life)
        {
            Console.WriteLine("Heal event");
            if (monster.npc.life + life >= monster.npc.lifeMax)
            {
                int heal = monster.npc.lifeMax - monster.npc.life;
                monster.npc.life = monster.npc.lifeMax;
                Console.WriteLine("Healed: {0}", heal);
                return heal;
            }
            else
            {
                int heal = life;
                int lifeLeft = monster.npc.lifeMax - (monster.npc.life + heal);
                Console.WriteLine("Healed: {0}", heal);
                Console.WriteLine(lifeLeft);
                monster.npc.life += heal;
                return heal;
            }
        }
    }
}
